package cn.uncode.baas.server.service;

import cn.uncode.baas.server.acl.token.AccessToken;
import cn.uncode.baas.server.exception.ValidateException;

import java.util.List;
import java.util.Map;

public interface IUserService {
	
	public static final String EMAIL_ACTIVATE = "activate";
	public static final String EMAIL_RESET = "resetpass";
	
	String signup(Map<String, Object> param) throws ValidateException, Exception;
	
	String login(Map<String, Object> param) throws ValidateException, Exception;
	
	int update(Map<String, Object> param) throws ValidateException, Exception;
	
	Map<String, Object> getUser(Map<String, Object> param);
	
	void logout(Map<String, Object> param);
	
	boolean checkToken(Map<String, Object> param);
	
	Map<String, Object> getToken(Map<String, Object> param);
	
	AccessToken getToken(String token);
	
	void sendMailAgain(Map<String, Object> param) throws ValidateException;
	
	boolean checkUsername(Map<String, Object> param) throws ValidateException;
	
	List<Map<String, Object>> selectUsers(Map<String, Object> param);

}
