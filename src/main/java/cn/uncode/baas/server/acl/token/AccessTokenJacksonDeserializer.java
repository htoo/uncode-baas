/*
 * Copyright 2006-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package cn.uncode.baas.server.acl.token;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.*;

/**
 * <p>
 * Provides the ability to deserialize JSON response into an {@link org.springframework.AccessToken.oauth2.common.OAuth2AccessToken} with jackson2 by implementing
 * {@link com.fasterxml.jackson.databind.JsonDeserializer}.
 * </p>
 * <p>
 * The expected format of the access token is defined by <a
 * href="http://tools.ietf.org/html/draft-ietf-oauth-v2-22#section-5.1">Successful Response</a>.
 * </p>
 *
 * @author wj.ye
 */
@SuppressWarnings("serial")
public final class AccessTokenJacksonDeserializer extends StdDeserializer<AccessToken> {

	public AccessTokenJacksonDeserializer() {
		super(AccessToken.class);
	}

	@Override
	public AccessToken deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException,
			JsonProcessingException {

		String tokenValue = null;
		String tokenType = null;
		Long expiresIn = null;
		Set<String> scope = null;
		Map<String, Object> additionalInformation = new LinkedHashMap<String, Object>();

		// TODO What should occur if a parameter exists twice
		while (jp.nextToken() != JsonToken.END_OBJECT) {
			String name = jp.getCurrentName();
			jp.nextToken();
			if (AccessToken.ACCESS_TOKEN.equals(name)) {
				tokenValue = jp.getText();
			}
			else if (AccessToken.TOKEN_TYPE.equals(name)) {
				tokenType = jp.getText();
			}
			else if (AccessToken.EXPIRES_IN.equals(name)) {
				expiresIn = jp.getLongValue();
			}
			else if (AccessToken.SCOPE.equals(name)) {
				String text = jp.getText();
				scope = parseParameterList(text);
			} else {
				additionalInformation.put(name, jp.readValueAs(Object.class));
			}
		}

		// TODO What should occur if a required parameter (tokenValue or tokenType) is missing?

		DefaultAccessToken accessToken = new DefaultAccessToken(tokenValue);
		accessToken.setTokenType(tokenType);
		if (expiresIn != null) {
			accessToken.setExpiration(new Date(System.currentTimeMillis() + (expiresIn * 1000)));
		}
		accessToken.setScope(scope);
		accessToken.setAdditionalInformation(additionalInformation);

		return accessToken;
	}
	
	private Set<String> parseParameterList(String values) {
		Set<String> result = new TreeSet<String>();
		if (values != null && values.trim().length() > 0) {
			// the spec says the scope is separated by spaces, but Facebook uses commas, so we'll include commas, too.
			String[] tokens = values.split("[\\s+,]");
			result.addAll(Arrays.asList(tokens));
		}
		return result;
	}
}